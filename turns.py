#!/usr/bin/python3

import math
import os
import re
import sys

from collections import defaultdict
from itertools import chain
from lxml import etree as ET
from typing import Dict, Generator, List, Optional

MIN_DIST_BTWN_POINTS = 10 # meters
MIN_DIST_BTWN_TURNS = 50 # meters
MIN_ANGLE_FOR_TURN = 25 # degrees
MIN_DIST_BTWN_SUMMITS = 10000 # meters

FILE_DISALLOWED_CHARS_RE = r"[^A-Za-z0-9-_]"

# Create GPX 1.1 files
GPX_1_1_NAMESPACE = "http://www.topografix.com/GPX/1/1"
ET.register_namespace("None", GPX_1_1_NAMESPACE)

SUMMIT = "Summit"
LEFT = "Left"
RIGHT = "Right"
STRAIGHT = "Straight"
SUPPORTED_NAMES = set([
    "1st Category",
    "2nd Category",
    "3rd Category",
    "4th Category",
    "Danger",
    "First Aid",
    "Food",
    "Hors Category",
    LEFT,
    RIGHT,
    "Sprint",
    STRAIGHT,
    SUMMIT,
    "Valley",
    "Water",
    "Generic"
])
DEFAULT_NAME = "Generic"
TURNS = set([LEFT, RIGHT, STRAIGHT])

class Coord:
    def __init__(self, lat, lon):
        """In degrees"""
        self.lat = lat
        self.lon = lon

    def __hash__(self):
        return int(self.lat * 32 + self.lon * 64)

    def __eq__(self, other):
        if not isinstance(other, Coord):
            return False
        return self.lat == other.lat and self.lon == other.lon

class TrkRte:
    def __init__(self, root_element : ET.Element, is_route : bool):
        """is_route means the segment is a route, else trk"""
        self.root_element = root_element
        self.is_route = is_route

num_files = 0
def get_next_file_id():
    global num_files
    num_files += 1
    return num_files

# Adapted from https://www.movable-type.co.uk/scripts/latlong.html
# Uses Haversine
def distance(c1 : Optional[Coord], c2 : Optional[Coord]) -> int:
    """Calc distance from c1 to c2

    Returns 0 if c1 or c2 are None"""

    if None in [c1, c2]:
        return 0

    R = 6371e3; # metres
    lat1r = c1.lat * math.pi/180; # radians
    lat2r = c2.lat * math.pi/180;
    dlatr = (c2.lat - c1.lat) * math.pi/180;
    dlonr = (c2.lon - c1.lon) * math.pi/180;

    a = (math.sin(dlatr/2)**2 +
         math.cos(lat1r) * math.cos(lat2r) *
         math.sin(dlonr/2)**2)

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    return R * c; # in metres

# Adapted from https://www.movable-type.co.uk/scripts/latlong.html
# Uses forward azimuth
def bearing(c1 : Optional[Coord], c2 : Optional[Coord]) -> int:
    """Bearing from c1 to c2 as compass angle (0 is North, 180 south)

    Returns 0 if c1 or c2 are None
    """

    if None in [c1, c2]:
        return 0

    lat1r = c1.lat * math.pi/180; # radians
    lat2r = c2.lat * math.pi/180;
    lon1r = c1.lon * math.pi/180;
    lon2r = c2.lon * math.pi/180;

    y = math.sin(lon2r - lon1r) * math.cos(lat2r)
    x = (math.cos(lat1r) * math.sin(lat2r) -
         math.sin(lat1r) * math.cos(lat2r) * math.cos(lon2r -lon1r))
    theta = math.atan2(y, x)

    return (theta * 180/math.pi + 360) % 360 # in degrees

def get_turn(prev : Coord,
             cur : Coord,
             next : Coord) -> int:
    """Angle between two lines"""
    cur_bearing = bearing(prev, cur)
    next_bearing = bearing(cur, next)
    return int((next_bearing - cur_bearing) % 360)

def get_coord(pt_ele : ET.Element) -> Coord:
    return Coord(float(pt_ele.attrib["lat"]),
                 float(pt_ele.attrib["lon"]))

def get_nearest_point(gpx : ET.ElementTree, wpt : ET.Element) -> ET.Element:
    nearest_point = None
    nearest_distance = 0

    wpt_coord = get_coord(wpt)

    for pt in chain(gpx.iter("{*}trkpt"), gpx.iter("{*}rtept")):
        pt_coord = get_coord(pt)
        dist = distance(pt_coord, wpt_coord)
        if (nearest_point is None or dist < nearest_distance):
            nearest_point = pt
            nearest_distance = dist

    return nearest_point

def get_wpt_table(gpx : ET.ElementTree) -> Dict[Coord, List[ET.Element]]:
    """Get dict from coord of nearest trkpt to nearest wpts"""
    tbl = defaultdict(list)
    for wpt in gpx.iter("{*}wpt"):
        trkpt = get_nearest_point(gpx, wpt)
        tbl[get_coord(trkpt)].append(wpt)
    tbl.setdefault(None)
    return tbl

def get_max_elevation(gpx : ET.ElementTree) -> float:
    """Gets highest elevation from trk/rtepts. 0 if none."""
    max_ele = 0.0
    # two passes bad
    for search in ["{*}trkpt", "{*}rtept"]:
        for pt in gpx.iter(search):
            ele = pt.findtext("{*}ele")
            if ele is not None:
                max_ele = max(max_ele, float(ele))
    return max_ele

def read_gpx(gpx_file : str) -> ET.ElementTree:
    parser = ET.XMLParser(remove_blank_text=True, recover=True)
    return ET.parse(gpx_file, parser)

def get_trkrtes(gpx : ET.ElementTree) -> Generator[TrkRte, None, None]:
    """Returns a generator of routes/track segments in order found in
    file"""
    for ele in gpx.getroot():
        if re.match(r"{.*}trk", ele.tag):
            yield TrkRte(ele, False)
        elif re.match(r"{.*}rte", ele.tag):
            yield TrkRte(ele, True)

def get_segments(trkrte : TrkRte) -> Generator[ET.Element, None, None]:
    if trkrte.is_route:
        yield trkrte.root_element
    else:
        for seg in trkrte.root_element.findall("{*}trkseg"):
            yield seg

def get_pts(seg : ET.Element,
            is_route : bool) -> Generator[ET.Element, None, None]:
    pt_search = "{*}rtept" if is_route else "{*}trkpt"
    for pt in seg.findall(pt_search):
        yield pt

def make_new_gpx(gpx : ET.ElementTree, trkrte : ET.Element) -> ET.ElementTree:
    """Creates a new GPX XML tree with a name derived from gpx or
    trkrte"""
    new_gpx = ET.ElementTree()
    root = ET.Element("gpx", nsmap = gpx.getroot().nsmap)
    root.set("version", "1.1")

    name_id = get_next_file_id()
    name = "Turns {}".format(name_id)

    trkrte_name = trkrte.root_element.findtext("{*}name")
    if trkrte_name is not None:
        name = trkrte_name
    else:
        gpx_name = gpx.findtext("{*}name")
        if gpx_name is not None:
            name = "{} Turns {}".format(gpx_name, name_id)

    ET.SubElement(root, "name").text = name

    return ET.ElementTree(root)

def combine_wpt_names(wpt_name : str, new_wpt_name : str) -> str:
    """Determine new name given old

    Does not change old if it is a turn. Otherwise combines either to a
    given name or the DEFAULT_NAME if there is a conflict. Returns new
    name."""

    if wpt_name in TURNS:
        return wpt_name

    if wpt_name is None or wpt_name == new_wpt_name:
        return new_wpt_name

    return DEFAULT_NAME

def build_trkrte(gpx : ET.ElementTree,
                 trkrte : TrkRte,
                 wpt_tbl : Dict[Coord, List[ET.Element]],
                 max_ele : float) -> ET.ElementTree:
    """Creates a new gpx ElementTree containing a copy of trkrte and
    with relevant wpts from wpt_tbl. Moves elements from gpx to new gpx,
    snapping to nearest track point.  max_ele is used to mark the summit
    and should be the highest elevation in the file."""

    new_gpx = make_new_gpx(gpx, trkrte)
    new_gpx.getroot().append(trkrte.root_element)

    # set of coords for which waypoints have been generated
    # can't have duplicates, and some tracks have duplicate points
    done_wpts = set()

    for seg in get_segments(trkrte):
        previous_point = None
        current_point = None
        next_point = None
        dist_since_turn = 0
        dist_since_summit = 0
        had_summit = False

        for pt in get_pts(seg, trkrte.is_route):
            pt_coord = get_coord(pt)

            # Start off assuming true and eliminate possibilities
            needs_turn = True

            needs_turn &= not (
                next_point is not None and
                distance(next_point, pt_coord) < MIN_DIST_BTWN_POINTS
            )

            previous_point = current_point
            current_point = next_point
            next_point = pt_coord

            needs_turn &= not (None in [previous_point, current_point])

            dist = distance(previous_point, current_point)
            dist_since_turn += dist
            dist_since_summit += dist

            turn = get_turn(previous_point, current_point, next_point)

            needs_turn &= (
                turn > MIN_ANGLE_FOR_TURN and
                turn < 360 - MIN_ANGLE_FOR_TURN and
                dist_since_turn >= MIN_DIST_BTWN_TURNS
            )

            # If we need a new way point, collect a list of descriptions
            # and the name from SUPPORTED_NAMES
            wpt_descs = []
            wpt_name = None

            # Collect turn info
            if needs_turn:
                direction = RIGHT if turn < 180 else LEFT
                dist_since_turn = 0
                wpt_name = direction
                wpt_descs.append("Turn {}".format(direction))

            # Add waypoint info
            if current_point is not None and current_point in wpt_tbl:
                for wpt in wpt_tbl[current_point]:
                    desc = wpt.findtext("{*}desc")
                    name = wpt.findtext("{*}name")

                    # If the name is not already a TCX point type, move the name to
                    # the description as this is what the Bolt will display.
                    if name not in SUPPORTED_NAMES:
                        desc = name
                        name = DEFAULT_NAME

                    wpt_descs.append(desc)
                    wpt_name = combine_wpt_names(wpt_name, name)

            # Mark summit
            pt_ele = pt.findtext("{*}ele")
            if (pt_ele is not None and
                float(pt_ele) == max_ele and
                (not had_summit or
                 dist_since_summit >= MIN_DIST_BTWN_SUMMITS)):

                wpt_descs.append(SUMMIT)
                wpt_name = combine_wpt_names(wpt_name, SUMMIT)
                dist_since_summit = 0
                had_summit = True

            if current_point is not None:
                wpt_pos = (current_point.lat, current_point.lon)
                if wpt_name is not None and wpt_pos not in done_wpts:
                    wpt_desc = ", ".join(wpt_descs)

                    wpt = ET.SubElement(new_gpx.getroot(), "wpt")
                    wpt.set("lat", str(current_point.lat))
                    wpt.set("lon", str(current_point.lon))
                    ET.SubElement(wpt, "name").text = wpt_name
                    ET.SubElement(wpt, "desc").text = wpt_desc

                    done_wpts.add(wpt_pos)

    return new_gpx

def make_turns_files(gpx : ET.ElementTree,
                     wpt_tbl : Dict[Coord, List[ET.Element]],
                     gpx_dir_out : str):
    """Creates new files int he gpx_dir_out directory for each track or
    route in the gpx file. Saves them with a name derived from the name
    of the track or the gpx file"""

    max_ele = get_max_elevation(gpx)

    for trkrte in get_trkrtes(gpx):
        new_gpx = build_trkrte(gpx, trkrte, wpt_tbl, max_ele)
        file_name = re.sub(FILE_DISALLOWED_CHARS_RE, "_",
                           new_gpx.findtext("name"))
        new_gpx.write(gpx_dir_out + os.sep + file_name + ".gpx",
                      pretty_print=True)

def process_file(gpx_file_in : str, gpx_dir_out : str):
    gpx = read_gpx(gpx_file_in)
    wpt_tbl = get_wpt_table(gpx)
    make_turns_files(gpx, wpt_tbl, gpx_dir_out)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: ./turns.py gpx_in gpx_out_directory")
        exit(-1)

    gpx_file_in = sys.argv[1]
    gpx_dir_out = sys.argv[2]

    if not gpx_file_in.endswith(".gpx") and not gpx_file_in.endswith(".GPX"):
        print(gpx_file_in, "does not looks like a GPX")
        exit(-1)

    if not os.path.isfile(gpx_file_in):
        print(gpx_file_in, "is not a file")
        exit(-1)

    if not os.path.isdir(gpx_dir_out):
        print(gpx_dir_out, "is not a directory")
        exit(-1)

    process_file(gpx_file_in, gpx_dir_out)

