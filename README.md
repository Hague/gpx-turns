
# GPX Turns

A script for adding turn cues to a GPX file, primarily for use with a
Wahoo ELEMNT Bolt.

## Usage

Requires lxml

    $ pip --user install lxml

Then run

    $ ./turns.py <gpx_file> <output_directory>

For each route or track in `gpx_file` it will create a separate file in
`output_directory` with the name of the route or track as specified in
the GPX file. If no name is given, it will use the name specified for
the whole file, plus "Turns id", where id is a number.

## What's in the Output GPX Files?

A single GPX route/track plus a sequence of waypoints. These waypoints
cue the left/right turns. Existing waypoints in `gpx_file` will be
snapped to a track point and split between the files according to the
nearest track/route point.

The waypoints appear in "route/track order" -- this seems to be
important for the ELEMNT Bolt.

Also marks highest point as "Summit", because why not. Especially when
your highest elevation is 61m.

The existing waypoints are snapped to track points because the Bolt only
appears to be able to handle them this way. Multiple waypoints that snap
to the same trackpoint will be merged, as the Bolt also doesn't seem to
like multiple way points on the same track point. It stops giving
directions at all when it finds waypoints it doesn't like.

This is all subject to testing -- i am still experimenting with this.

## Limitations

The script has no knowledge of the underlying road network. The cues are
best as a reminder to look at the map. They cannot be trusted solely as
audio cues.

Turns are based on whether the GPX track/route changes direction, not on
whether you have to move from one road to another.

Most of the time they're ok. Picture below using
[Viking](https://github.com/viking-gps/viking).

![Example of OK cues](turns.png)

But sometimes they're screwy

![Example of odd cues](turns-weirdness.png)

In the above case, the uppermost left turn is a kink in the route, while
the "main" turn direction is actually to the right.

This happens because the script

* only looks at track/route points more than 10m apart, and
* only outputs a new turn if the last one was more than 50m ago.

The above can be tweaked via global variables, as well as the change in
bearing required to be considered a turn.

## Use with Wahoo ELEMNT Bolt

Mount your device via MTP. For this i use `jmtpfs`

    $ jmptfs ~/mnt

Run the script and tell it to output to the `routes` folder.

    $ ./turns.py MyPlanned.gpx "~/mnt/USB storage/routes/"

Then run "sync" on the Wahoo "routes" page, choose your route, and go.

## Technical Things

The Wahoo ELEMNT Bolt treats all waypoints as cue points, seemingly in
the order they are found in the file. The waypoint names supported with
an icon seem to be the
[TCX](https://www8.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd)
ones:

* 1st Category
* 2nd Category
* 3rd Category
* 4th Category
* Danger
* First Aid
* Food
* Hors Category
* Left
* Right
* Sprint
* Straight
* Summit
* Valley
* Water
* Generic

The `desc` is the displayed text.

    <wpt lat="51.306824" lon="-0.489758">
        <name>Left</name>
        <desc>Turn Left</desc>
    </wpt>

I've read somewhere that GPX files can't have cuesheets, and that
they're only supported in TCX. The Wahoo seems happy to treat them as
cue points though.
